<?php

declare(strict_types=1);

namespace ThrustbitTests\Firewall\Unit\Factory;

use Thrustbit\Firewall\Factory\Registry;
use Thrustbit\Firewall\Foundation\Contracts\Registry\FirewallRegistry;
use ThrustbitTests\Firewall\TestCase;

class RegistryTest extends TestCase
{
    /**
     * @test
     * @expectedException \Assert\AssertionFailedException
     */
    public function it_raise_exception_when_bootstraps_is_empty(): void
    {
        new Registry([]);
    }

    /**
     * @test
     * @expectedException \Assert\AssertionFailedException
     */
    public function it_raise_exception_when_bootstrap_does_not_implement_firewall_registry_interface(): void
    {
        new Registry([new \stdClass()]);
    }

    /**
     * @test
     */
    public function it_return_bootstraps_sorted_by_priority(): void
    {
        $registry = new Registry($this->getUnsortedBootstraps());

        $bootstraps = $registry->getBootstraps();


        $first = array_shift($bootstraps);
        $this->assertSame(-5, $first->getPriority());

        $first = array_shift($bootstraps);
        $this->assertSame(0, $first->getPriority());

        $first = array_shift($bootstraps);
        $this->assertSame(5, $first->getPriority());

        $this->assertEmpty($bootstraps);
    }


    private function getUnsortedBootstraps(): array
    {
        return [
            new class implements FirewallRegistry{
                public function getPriority(): int
                {
                    return 5;
                }
            },
            new class implements FirewallRegistry{
                public function getPriority(): int
                {
                    return -5;
                }
            },
            new class implements FirewallRegistry{
                public function getPriority(): int
                {
                    return 0;
                }
            },
        ];
    }
}