<?php

declare(strict_types=1);

namespace ThrustbitTests\Firewall\Unit\Foundation\Registry;

use Thrustbit\Firewall\Foundation\Registry\AnonymousRequestRegistry;
use Thrustbit\Security\Application\Values\SecurityKey\AnonymousKey;

class AnonymousRequestRegistryTest extends FactoryTestCase
{
    /**
     * @test
     */
    public function it_does_not_register_when_firewall_is_not_anonymous(): void
    {
        $this->factory->expects($this->atLeastOnce())->method('context')->willReturn($this->context);
        $this->context->expects($this->atLeastOnce())->method('isAnonymous')->willReturn(false);

        $this->factory->expects($this->never())->method('key');

        $this->assertEquals($this->factory, $this->dispatch());
    }

    /**
     * @test
     */
    public function it_register_when_firewall_is_anonymous(): void
    {
        $this->context->expects($this->once())->method('isAnonymous')->willReturn(true);

        $this->factory->expects($this->any())->method('context')->willReturn($this->context);
        $this->factory->expects($this->any())->method('key')->willReturn('foo_bar');
        $this->factory->expects($this->once())->method('registerService');

        $this->context->expects($this->atLeastOnce())->method('anonymousKey')->willReturn(
            $anonKey = new AnonymousKey('bar')
        );

        $this->assertEquals($this->factory, $this->dispatch());

        $this->assertTrue($this->app->bound('firewall.anonymous.default_authentication_listener.foo_bar'));
        $this->assertTrue($this->app->bound('firewall.anonymous.default_authentication_provider.foo_bar'));
    }

    private function dispatch()
    {
        return (new AnonymousRequestRegistry($this->app))->compose($this->factory, function () {
            return $this->factory;
        });
    }
}