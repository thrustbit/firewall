<?php

declare(strict_types=1);

namespace ThrustbitTests\Firewall\Unit\Foundation\Registry;

use Thrustbit\Firewall\Exceptions\FirewallException;
use Thrustbit\Firewall\Factory\Aggregate;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Foundation\Registry\AuthenticationProviderRegistry;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\Authenticatable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\AuthenticationProvider;

class AuthenticationProviderRegistryTest extends FactoryTestCase
{
    /**
     * @test
     */
    public function it_register_authentication_providers(): void
    {
        $ag = new Aggregate();
        ($ag)((new PayloadFactory())->setProvider('auth_provider_id'));

        $this->factory->expects($this->once())->method('aggregate')->willReturn($ag);

        $factory = $this->dispatch();

        $this->assertTrue($this->app->bound(Authenticatable::class));
        $this->assertEquals($this->factory, $factory);
    }

    /**
     * @test
     * @expectedException \Thrustbit\Firewall\Exceptions\FirewallException
     */
    public function it_raise_exception_when_no_authentication_provider_provided(): void
    {
        $ag = new Aggregate();
        $this->factory->expects($this->once())->method('aggregate')->willReturn($ag);

        $this->dispatch();
    }

    public function dispatch()
    {
        return (new AuthenticationProviderRegistry($this->app))
            ->compose($this->factory, function () {
                return $this->factory;
            });
    }
}