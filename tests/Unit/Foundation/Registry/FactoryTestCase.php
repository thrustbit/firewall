<?php

declare(strict_types=1);

namespace ThrustbitTests\Firewall\Unit\Foundation\Registry;

use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Foundation\Contracts\Factory\FirewallContext;
use ThrustbitTests\Firewall\TestCase;

class FactoryTestCase extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Factory
     */
    protected $factory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|FirewallContext
     */
    protected $context;

    public function setUp()
    {
        parent::setup();

        $this->factory = $this->getMockBuilder(Factory::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->context = $this->getMockForAbstractClass(FirewallContext::class);
    }
}