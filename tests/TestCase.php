<?php

declare(strict_types=1);

namespace ThrustbitTests\Firewall;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\Container as ContainerContract;
use Illuminate\Foundation\Application;

class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Application
     */
    protected $app;

    public function setup()
    {
        $app = new Application();
        $app->bind(ContainerContract::class, Container::class);

        $this->app = $app;
    }
}