<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Factory;

use Assert\Assertion;

class Registry
{
    /**
     * @var array
     */
    private $bootstraps = [];

    public function __construct(array $bootstraps)
    {
        Assertion::notEmpty($bootstraps);

        $this->setBootstraps($bootstraps);
    }

    final public function getBootstraps(): array
    {
        return $this->bootstraps;
    }

    private function setBootstraps(array $bootstraps): void
    {
        $this->bootstraps = $bootstraps;
    }
}