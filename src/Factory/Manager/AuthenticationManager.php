<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Factory\Manager;

use Illuminate\Contracts\Container\Container;
use Thrustbit\Firewall\Exceptions\FirewallException;
use Thrustbit\Firewall\Factory\AuthenticationService;
use Thrustbit\Firewall\Factory\Config\ConfigChecker;
use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Thrustbit\Firewall\Foundation\Contracts\Factory\MutableFirewallContext;
use Thrustbit\Security\Application\Values\SecurityKey\ContextKey;

class AuthenticationManager
{
    /**
     * @var ConfigChecker
     */
    private $configChecker;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    protected $firewall = [];

    /**
     * @var array
     */
    protected $factories = [];

    public function __construct(ConfigChecker $configChecker, Container $container)
    {
        $this->configChecker = $configChecker;
        $this->container = $container;
    }

    public function create(string $name): Factory
    {
        if (isset($this->firewall[$name])) {
            return $this->firewall[$name];
        }

        $this->requireFirewall($name);

        $config = $this->configChecker->getFirewall($name);

        return $this->firewall[$name] = $this->getFactory($name, $config);
    }

    /**
     * @param string $firewallKey
     * @param string $serviceKey
     * @param string|callable $serviceFactory
     *
     * @return AuthenticationManager
     */
    public function extend(string $firewallKey, string $serviceKey, $serviceFactory): AuthenticationManager
    {
        throw new \RuntimeException('todo');

        $this->requireFirewall($firewallKey);

        if (!is_callable($serviceFactory) && !is_string($serviceFactory)) {
            throw new FirewallException('Extend service from authentication manager must be a callable or a string');
        }

        $this->factories[$firewallKey][$serviceKey] = $serviceFactory;

        return $this;
    }

    public function hasFirewall(string $name): bool
    {
        return $this->configChecker->hasFirewall($name);
    }

    private function getFactory(string $name, array $config): Factory
    {
        return new Factory(
            new ContextKey($name),
            $this->resolveFactories($name, $config),
            $this->resolveContext($config['context']),
            $this->configChecker->getUserProviders()
        );
    }

    private function resolveFactories(string $name, array $config): AuthenticationService
    {
        $factories = [];

        // extends not handle
        // need to merge services with factories
        // but we need to prioritize when multiple auth factories share same position

        foreach ((array)$config['services'] as $serviceKey => $factoryId) {
            $factories [] = is_callable($factoryId)
                ? $factoryId($this->container)
                : $this->container->make($factoryId);
        }

        if (empty($factories)) {
            throw new FirewallException(
                sprintf('No authentication service factory provided for firewall %s', $name)
            );
        }

        return new AuthenticationService($factories);
    }

    private function resolveContext($context): FirewallContext
    {
        $context = is_callable($context)
            ? $context($this->container->make(FirewallContext::class))
            : $this->container->make($context);

        if ($context instanceof MutableFirewallContext) {
            return $context->toImmutable();
        }

        return $context;
    }

    private function requireFirewall(string $name): void
    {
        if (!$this->hasFirewall($name)) {
            throw new FirewallException(
                sprintf('Firewall name %s does not exist', $name)
            );
        }
    }
}