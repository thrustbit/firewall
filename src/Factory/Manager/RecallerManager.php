<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Factory\Manager;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Cookie\QueueingFactory;
use Illuminate\Contracts\Foundation\Application;
use Thrustbit\Firewall\Exceptions\FirewallException;
use Thrustbit\Firewall\Factory\Payload\PayloadRecaller;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\Firewall;
use Thrustbit\Security\Application\Values\SecurityKey\RecallerKey;
use Thrustbit\Security\Infrastructure\Services\Recaller\SimpleRecallerService;

class RecallerManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $services = [];

    /**
     * @var array
     */
    private $callback = [];

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    // use by auth service
    public function create(PayloadRecaller $payload)
    {
        if ($this->hasService($payload->recallerKey)) {
            return $this->getService($payload->recallerKey);
        }

        $recaller = $payload->recallerKey->getKey();

        return $this->services[$recaller] = $this->registerSimpleService($payload);
    }

    protected function registerSimpleService(PayloadRecaller $payload): string
    {
        $serviceId = 'firewall.simple_recaller_service.' . $payload->recallerKey->getKey();

        $this->container->bind($serviceId, function (Application $app) use ($payload) {
            return new SimpleRecallerService(
                $payload->recallerKey,
                $payload->service->firewallKey,
                $app->make(QueueingFactory::class),
                $app->make($payload->service->userProviderId)
            );
        });

        $this->setServiceOnResolvingListener($serviceId, $payload->listenerId);

        return $serviceId;
    }

    // use by recaller auth factory
    public function getService(RecallerKey $recallerKey): string
    {
        if ($this->hasService($recallerKey)) {
            return $this->services[$recallerKey->getKey()];
        }

        throw new FirewallException(
            sprintf('No recaller service registered with recaller %s', $recallerKey->getKey())
        );
    }

    public function hasService(RecallerKey $key): bool
    {
        return isset($this->services[$key->getKey()]);
    }

    private function setServiceOnResolvingListener(string $serviceId, string $listenerId): void
    {
        $this->container->resolving($listenerId,
            function (Firewall $firewall, Application $app) use ($serviceId) {
                if (!method_exists($firewall, 'setRecaller')) {
                    throw new FirewallException(
                        sprintf('Class %s must implement "setRecaller" method.', get_class($firewall))
                    );
                }

                $firewall->setRecaller($app->make($serviceId));
            });
    }
}