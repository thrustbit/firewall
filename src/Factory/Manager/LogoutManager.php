<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Factory\Manager;

use Illuminate\Contracts\Container\Container;
use Thrustbit\Firewall\Exceptions\FirewallException;

class LogoutManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $services = [];

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    // use by registry
    public function addService(array $payload)
    {
        $this->services += $payload;
    }

    // use by auth service
    public function addHandler(string $handler, string $serviceKey): LogoutManager
    {
        if ($this->hasService($serviceKey)) {

            if (!isset($this->services[$serviceKey]['handlers'])) {
                throw new FirewallException(
                    sprintf('Missing "handlers" key for logout payload in context for service key %s',
                        $serviceKey)
                );
            }

            $this->services[$serviceKey]['handlers'] = [$handler];

            return $this;
        }

        throw new FirewallException(
            sprintf('Logout manager can not locate service key %s', $serviceKey)
        );
    }

    final public function getHandlers(string $serviceKey): array
    {
        if ($this->hasService($serviceKey)) {
            $handlers = $this->services[$serviceKey]['handlers'];

            if (empty($handlers)) {
                throw new FirewallException(
                    sprintf('No logout handler has been registered for service key %s', $serviceKey)
                );
            }

            return $handlers;
        }

        throw new FirewallException(
            sprintf('Logout manager can not locate service key %s', $serviceKey)
        );
    }

    public function hasService(string $serviceKey): bool
    {
        return isset($this->services[$serviceKey]);
    }
}