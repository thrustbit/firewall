<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Factory;

use Illuminate\Support\Collection;
use Thrustbit\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;

class AuthenticationService
{
    /**
     * @var Collection
     */
    private $services;

    public function __construct(array $services = null)
    {
        $this->services = new Collection($services ?? []);
    }

    public function add(AuthenticationServiceFactory $service): AuthenticationService
    {
        $this->services->push($service);

        return $this;
    }

    final public function filterByPosition(string $servicePosition): Collection
    {
        return $this->services->filter(
            function (AuthenticationServiceFactory $serviceFactory) use ($servicePosition) {
                return $servicePosition === $serviceFactory->position();
            });
    }
}