<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Factory;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Thrustbit\Firewall\Exceptions\FirewallException;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Thrustbit\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Thrustbit\Security\Application\Values\SecurityKey\ContextKey;

class Factory
{
    /**
     * @var AuthenticationService
     */
    private $services;

    /**
     * @var ContextKey
     */
    private $firewallKey;

    /**
     * @var FirewallContext
     */
    private $context;

    /**
     * @var array
     */
    private $userProvidersIds = [];

    /**
     * @var Aggregate
     */
    private $aggregate;

    /**
     * @var string|null
     */
    private $entrypoint;

    /**
     * @var Request
     */
    private $request;

    public function __construct(ContextKey $contextKey,
                                AuthenticationService $services,
                                FirewallContext $context,
                                array $userProvidersIds)
    {
        $this->firewallKey = $contextKey;
        $this->services = $services;
        $this->context = $context;
        $this->userProvidersIds = $userProvidersIds;
        $this->aggregate = new Aggregate();
    }

    public function make(string $position): Collection
    {
        return $this->services->filterByPosition($position);
    }

    public function registerService(PayloadFactory $payloadFactory): void
    {
        ($this->aggregate)($payloadFactory);
    }

    public function entrypoint(): ?string
    {
        return $this->entrypoint;
    }

    public function setEntrypoint(string $entrypoint): void
    {
        if (null !== $this->entrypoint) {
            throw new FirewallException('Entrypoint already set'); // checkMe
        }

        $this->entrypoint = $entrypoint;
    }

    public function key(): string
    {
        if (!$this->hasSameContext() && !$this->context()->isStateless()) {
            return $this->context->contextKey()->getKey();
        }

        return $this->firewallKey->getKey();
    }

    public function originalKey(): ContextKey
    {
        return $this->firewallKey;
    }

    public function hasSameContext(): bool
    {
        return $this->context->contextKey()->sameValueAs($this->firewallKey);
    }

    public function userProvider(string $userProvider = null): string
    {
        $id = $userProvider ?? $this->context->userProviderKey();

        if (!array_key_exists($id, $this->userProvidersIds)) {
            throw new FirewallException(
                sprintf('User provider with id "%s" not found', $id));
        }

        return $this->userProvidersIds[$id];
    }

    public function userProvidersIds(): array
    {
        return array_flatten($this->userProvidersIds);
    }

    public function context(): FirewallContext
    {
        return $this->context;
    }

    public function aggregate(): Aggregate
    {
        return $this->aggregate;
    }

    public function setRequest(Request $request): Factory
    {
        $this->request = $request;

        return $this;
    }

    public function serviceMatchRequest(AuthenticationServiceFactory $serviceFactory): bool
    {
        if (!$this->request) {
            throw new FirewallException('No request set on Factory');
        }

        if (null !== $authRequest = $serviceFactory->matcher()) {
            return $authRequest->matches($this->request);
        }

        return true;
    }

    final public function middleware(): array
    {
        return $this->aggregate()->listeners();
    }
}