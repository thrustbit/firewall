<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Factory\Payload;

use Thrustbit\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;

class PayloadService
{
    /**
     * @var ProviderKey
     */
    public $firewallKey;

    /**
     * @var FirewallContext
     */
    public $context;

    /**
     * @var string
     */
    public $userProviderId;

    /**
     * @var string
     */
    public $entrypoint;

    /**
     * PayloadService constructor.
     *
     * @param ProviderKey $providerKey
     * @param FirewallContext $context
     * @param string $userProviderId
     * @param string|null $entrypoint
     */
    public function __construct(ProviderKey $providerKey, FirewallContext $context, string $userProviderId, string $entrypoint = null)
    {
        $this->firewallKey = $providerKey;
        $this->context = $context;
        $this->userProviderId = $userProviderId;
        $this->entrypoint = $entrypoint;
    }
}