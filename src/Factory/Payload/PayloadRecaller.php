<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Factory\Payload;

use Thrustbit\Security\Application\Values\SecurityKey\RecallerKey;

class PayloadRecaller
{
    /**
     * @var PayloadService
     */
    public $service;

    /**
     * @var RecallerKey
     */
    public $recallerKey;

    /**
     * @var string
     */
    public $listenerId;

    public function __construct(PayloadService $service, RecallerKey $recallerKey, string $listenerId)
    {
        $this->service = $service;
        $this->recallerKey = $recallerKey;
        $this->listenerId = $listenerId;
    }
}