<?php

use Thrustbit\Security\Application\Values\SecurityIdentifier;
use Thrustbit\Security\Domain\User\UserSecurity;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Tokenable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Authorizable;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Contract\Grantable;

if (!function_exists('getToken')) {

    function getToken(): ?Tokenable
    {
        return app(Storage::class)->getToken();
    }
}

if (!function_exists('getUser')) {

    /**
     * @return UserSecurity|SecurityIdentifier
     */
    function getUser()
    {
        return getToken()->getUser();
    }
}

if (!function_exists('isAnonymous')) {

    function isAnonymous(Tokenable $token = null): bool
    {
        return getTrustResolver()->isAnonymous($token);
    }
}

if (!function_exists('isFullFledged')) {

    function isFullFledged(Tokenable $token = null): bool
    {
        return getTrustResolver()->isFullFledged($token);
    }
}

if (!function_exists('isRememberMe')) {

    function isRememberMe(Tokenable $token = null): bool
    {
        return getTrustResolver()->isRememberMe($token);
    }
}

if (!function_exists('hasRole')) {

    function hasRole(Tokenable $token, string $role, $object = null): bool
    {
        return getAuthorizer()->decide($token, [$role], $object ?? request());
    }
}

if (!function_exists('hasRoles')) {

    function hasRoles(Tokenable $token, array $roles, $object = null): bool
    {
        return getAuthorizer()->decide($token, $roles, $object ?? request());
    }
}

if (!function_exists('getAuthorizer')) {

    function getAuthorizer(): Authorizable
    {
        return app(Authorizable::class);
    }
}

if (!function_exists('isGranted')) {

    function isGranted($attribute, $subject = null): bool
    {
        return app(Grantable::class)->isGranted((array)$attribute, $subject);
    }
}

if (!function_exists('isNotGranted')) {

    function isNotGranted($attribute, $subject = null): bool
    {
        return !isGranted($attribute, $subject);
    }
}

if (!function_exists('getTrustResolver')) {

    function getTrustResolver(): TrustResolver
    {
        return app(TrustResolver::class);
    }
}
