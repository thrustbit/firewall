<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Exceptions;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthorizationException;
use Thrustbit\Security\Domain\User\Exceptions\InsufficientAuthentication;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;

class AuthorizationExceptionHandler
{
    /**
     * @var Storage
     */
    private $tokenStorage;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    public function __construct(Storage $tokenStorage, TrustResolver $trustResolver)
    {
        $this->tokenStorage = $tokenStorage;
        $this->trustResolver = $trustResolver;
    }

    public function handle(AuthorizationException $authorizationException,
                           Request $request,
                           ContextualSecurityHandler $securityHandler): Response
    {
        $token = $this->tokenStorage->getToken(); // required

        if (!$this->trustResolver->isFullFledged($token)) {
            return $this->whenUserIsNotFullyAuthenticated($request, $authorizationException, $securityHandler);
        }

        return $this->whenUserIsNotGranted($request, $authorizationException, $securityHandler);
    }

    protected function whenUserIsNotFullyAuthenticated(Request $request,
                                                       AuthorizationException $exception,
                                                       ContextualSecurityHandler $contextual): Response
    {
        $message = 'Full authentication is required to access this resource.';
        $authException = new InsufficientAuthentication($message, 0, $exception);

        return $contextual->startAuthentication($request, $authException);
    }

    protected function whenUserIsNotGranted(Request $request,
                                            AuthorizationException $exception,
                                            ContextualSecurityHandler $contextual): Response
    {
        if (null !== $denied = $contextual->deniedHandler()) {
            return $denied->handle($request, $exception);
        }

        throw $exception;
    }
}