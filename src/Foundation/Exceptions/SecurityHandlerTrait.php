<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Exceptions;

use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Firewall\Foundation\Contracts\Exceptions\ExceptionRegistry;
use Thrustbit\Security\Application\Exceptions\SecurityException;

trait SecurityHandlerTrait
{
    /**
     * @var ExceptionRegistry|SecurityExceptionHandler
     */
    protected $handler;

    public function render($request, \Exception $exception): Response
    {
        if ($exception instanceof SecurityException) {
            return $this->handler->handle($request, $exception);
        }

        return parent::render($request, $exception);
    }

    public function setFirewallExceptionHandler(ExceptionRegistry $handler): void
    {
        $this->handler = $handler;
    }
}