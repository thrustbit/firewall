<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Exceptions;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Http\Response\Authorization\Contract\AuthorizationDenied;
use Thrustbit\Security\Application\Http\Response\Entrypoint\Contract\Entrypoint;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Domain\User\Exceptions\UserStatusException;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;

class ContextualSecurityHandler
{
    /**
     * @var Storage
     */
    private $tokenStorage;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * @var bool
     */
    private $stateless;

    /**
     * @var Entrypoint
     */
    private $entrypoint;

    /**
     * @var AuthorizationDenied
     */
    private $deniedHandler;

    public function __construct(Storage $tokenStorage,
                                ProviderKey $providerKey,
                                TrustResolver $trustResolver,
                                bool $stateless,
                                Entrypoint $entrypoint = null,
                                AuthorizationDenied $deniedHandler = null)
    {
        $this->tokenStorage = $tokenStorage;
        $this->providerKey = $providerKey;
        $this->trustResolver = $trustResolver;
        $this->stateless = $stateless;
        $this->entrypoint = $entrypoint;
        $this->deniedHandler = $deniedHandler;
    }

    public function startAuthentication(Request $request, AuthenticationException $exception): Response
    {
        if (!$this->entrypoint) {
            throw $exception;
        }

        if ($exception instanceof UserStatusException) {
            $this->tokenStorage->setToken(null);
        }

        return $this->entrypoint->startAuthentication($request, $exception);
    }

    public function setEntrypoint(Entrypoint $entrypoint): ContextualSecurityHandler
    {
        $this->entrypoint = $entrypoint;

        return $this;
    }

    public function entrypointHandler(): ?Entrypoint
    {
        return $this->entrypoint;
    }

    public function setAuthorizationDenied(AuthorizationDenied $authorizationDenied): ContextualSecurityHandler
    {
        $this->deniedHandler = $authorizationDenied;

        return $this;
    }

    public function deniedHandler(): ?AuthorizationDenied
    {
        return $this->deniedHandler;
    }
}