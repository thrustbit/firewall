<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Exceptions;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;

class AuthenticationExceptionHandler
{
    public function handle(AuthenticationException $authenticationException,
                           Request $request,
                           ContextualSecurityHandler $securityHandler): Response
    {
        return $securityHandler->startAuthentication($request, $authenticationException);
    }
}