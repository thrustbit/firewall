<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Exceptions;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Firewall\Foundation\Contracts\Exceptions\ExceptionRegistry;
use Thrustbit\Security\Application\Exceptions\AuthenticationException;
use Thrustbit\Security\Application\Exceptions\AuthorizationException;
use Thrustbit\Security\Application\Exceptions\SecurityException;

class SecurityExceptionHandler implements ExceptionRegistry
{
    /**
     * @var ContextualSecurityHandler
     */
    private $securityHandler;

    /**
     * @var AuthenticationExceptionHandler
     */
    private $authenticationExceptionHandler;

    /**
     * @var AuthorizationExceptionHandler
     */
    private $authorizationExceptionHandler;

    public function __construct(ContextualSecurityHandler $securityHandler,
                                AuthenticationExceptionHandler $authenticationExceptionHandler,
                                AuthorizationExceptionHandler $authorizationExceptionHandler)
    {
        $this->securityHandler = $securityHandler;
        $this->authenticationExceptionHandler = $authenticationExceptionHandler;
        $this->authorizationExceptionHandler = $authorizationExceptionHandler;
    }

    public function handle(Request $request, SecurityException $securityException): Response
    {
        if ($securityException instanceof AuthenticationException) {
            return $this->authenticationExceptionHandler->handle($securityException, $request, $this->securityHandler);
        }

        if ($securityException instanceof AuthorizationException) {
            return $this->authorizationExceptionHandler->handle($securityException, $request, $this->securityHandler);
        }

        throw $securityException;
    }

    public function __call(string $method, array $arguments)
    {
        return call_user_func_array([$this->securityHandler, $method], $arguments);
    }
}