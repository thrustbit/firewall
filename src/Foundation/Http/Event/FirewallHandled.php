<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Http\Event;

class FirewallHandled
{
    /**
     * @var array
     */
    private $middleware;

    /**
     * @var string
     */
    private $name;

    /**
     * FirewallHandled constructor.
     *
     * @param string $name
     * @param array $middleware
     */
    public function __construct(string $name, array $middleware)
    {
        $this->name = $name;
        $this->middleware = $middleware;
    }

    public function middleware(): array
    {
        return $this->middleware;
    }

    public function name(): string
    {
        return $this->name;
    }
}