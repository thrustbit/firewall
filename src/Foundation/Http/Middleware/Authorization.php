<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Http\Middleware;

use Illuminate\Http\Request;
use Thrustbit\Security\Infrastructure\Guard\Authorizer;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard;

class Authorization
{
    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var Authorizer
     */
    private $authorizer;

    public function __construct(Guard $guard, Authorizer $authorizer)
    {
        $this->guard = $guard;
        $this->authorizer = $authorizer;
    }

    public function handle(Request $request, \Closure $next, ... $attributes)
    {
        $token = $this->guard->storage()->required();

        if (!$token->isAuthenticated()) {
            $this->guard->storage()->put(
                $token = $this->guard->authenticate($token)
            );
        }

        $this->authorizer->requireGranted($token, $attributes, $request);

        return $next($request);
    }
}