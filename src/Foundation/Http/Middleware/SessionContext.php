<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Http\Middleware;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\TerminableInterface;
use Thrustbit\Security\Application\Http\Request\Event\ContextEventable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;

class SessionContext implements TerminableInterface
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * @var ContextEventable
     */
    private $event;

    /**
     * @var Storage
     */
    private $tokenStorage;

    public function __construct(Dispatcher $dispatcher, TrustResolver $trustResolver, Storage $tokenStorage)
    {
        $this->dispatcher = $dispatcher;
        $this->trustResolver = $trustResolver;
        $this->tokenStorage = $tokenStorage;
    }

    public function handle(Request $request, \Closure $next)
    {
        $this->dispatcher->listen(ContextEventable::class, [$this, 'onSecurityEvent']);

        return $next($request);
    }

    public function onSecurityEvent(ContextEventable $event): void
    {
        if ($this->event) {
            throw new \RuntimeException('Only one session context can run per request');
        }

        $this->event = $event;
    }


    public function terminate(SymfonyRequest $request, Response $response)
    {
        if (!$this->event) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        $sessionKey = $this->event->sessionKey();

        if (!$token || $this->trustResolver->isAnonymous($token)) {
            $request->session()->forget($sessionKey);
        } else {
            $request->session()->put($sessionKey, serialize($token));
        }
    }
}