<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Http\Middleware;

use Illuminate\Http\Request;
use Thrustbit\Firewall\Foundation\Contracts\Strategy\FirewallStrategy;

class Firewall
{
    /**
     * @var FirewallStrategy
     */
    private $strategy;

    public function __construct(FirewallStrategy $strategy)
    {
        $this->strategy = $strategy;
    }

    public function handle(Request $request, \Closure $next)
    {
        $this->strategy->delegateHandling($request);

        return $next($request);
    }
}