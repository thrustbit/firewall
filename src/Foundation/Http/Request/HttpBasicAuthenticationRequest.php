<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Http\Request;

use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\Request;
use Thrustbit\Security\Application\Http\Request\Firewall\Contract\AuthenticationRequest;
use Thrustbit\Security\Domain\User\Values\ClearPassword;
use Thrustbit\Security\Domain\User\Values\EmailAddress;

class HttpBasicAuthenticationRequest implements AuthenticationRequest
{
    public function extract(IlluminateRequest $request): array
    {
        return [
            EmailAddress::fromString($request->headers->get('PHP_AUTH_USER')),
            new ClearPassword($request->headers->get('PHP_AUTH_PW'))
        ];
    }

    public function matches(Request $request)
    {
        return null === $request->headers->get('PHP_AUTH_USER');
    }
}