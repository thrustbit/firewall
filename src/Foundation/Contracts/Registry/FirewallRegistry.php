<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Contracts\Registry;

interface FirewallRegistry
{
    const BOOTSTRAP_PRIORITY = -100;

    const SERIALIZATION_PRIORITY = -50;

    const AUTHENTICATION_SERVICE_PRIORITY = 0;

    const ANONYMOUS_PRIORITY = 50;

    const EXCEPTION_PRIORITY = 100;

    public function getPriority(): int;
}