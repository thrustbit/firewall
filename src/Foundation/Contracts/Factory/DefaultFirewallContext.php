<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Contracts\Factory;

use Thrustbit\Security\Application\Http\Response\Authorization\DefaultAuthorizationDenied;
use Thrustbit\Security\Application\Http\Response\Entrypoint\DefaultFormEntrypoint;
use Thrustbit\Security\Application\Values\SecurityKey\AnonymousKey;
use Thrustbit\Security\Application\Values\SecurityKey\ContextKey;

class DefaultFirewallContext implements FirewallContext
{
    /**
     * @var string
     */
    protected $anonymousKey = 'my_anonymous_key';

    /**
     * @var string
     */
    protected $contextKey = 'my_context_key';

    /**
     * @var bool
     */
    protected $anonymous = false;

    /**
     * @var bool
     */
    protected $stateless = false;

    /**
     * @var string
     */
    protected $entrypoint = DefaultFormEntrypoint::class;

    /**
     * @var string
     */
    protected $deniedHandler = DefaultAuthorizationDenied::class;

    /**
     * @var string
     */
    protected $userProvider = 'my_user_provider_id';

    /**
     * @var array
     */
    protected $recaller = [];

    /**
     * @var array
     */
    protected $logout = [];

    public function anonymousKey(): ?AnonymousKey
    {
        return new AnonymousKey($this->anonymousKey);
    }

    public function isAnonymous(): bool
    {
        return $this->anonymous;
    }

    public function isStateless(): bool
    {
        return $this->stateless;
    }

    public function contextKey(): ContextKey
    {
        return new ContextKey($this->contextKey);
    }

    public function entryPoint(): ?string
    {
        return $this->entrypoint;
    }

    public function deniedHandler(): ?string
    {
        return $this->deniedHandler;
    }

    public function userProviderKey(): string
    {
        return $this->userProvider;
    }

    public function hasRecaller(): bool
    {
        return !empty($this->recaller);
    }

    public function recaller(): array
    {
        return $this->recaller;
    }

    public function logout(): array
    {
        return $this->logout;
    }

    public function toImmutable(): FirewallContext
    {
        $self = new self();

        foreach (get_object_vars($this) as $key => $value) {
            $self->$key = $value;
        }

        return $self;
    }
}