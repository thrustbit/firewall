<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Contracts\Factory;

interface MutableFirewallContext extends FirewallContext
{
    public function setAnonymousKey(string $anonymousKey): MutableFirewallContext;

    public function setAnonymous(bool $anonymous): MutableFirewallContext;

    public function setStateless(bool $stateless): MutableFirewallContext;

    public function setContextKey(string $contextKey): MutableFirewallContext;

    public function setEntryPoint(string $entryPoint): MutableFirewallContext;

    public function setDeniedHandler(string $deniedHandler): MutableFirewallContext;

    public function addRecallerKey(string $serviceKey, string $recallerKey): MutableFirewallContext;

    public function addLogout(array $logout): MutableFirewallContext;
}