<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Contracts\Factory;

use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Factory\Payload\PayloadService;

interface AuthenticationServiceFactory
{
    public function register(PayloadService $payload): PayloadFactory;

    public function position(): string;

    public function key(): string;

    public function userProviderKey(): ?string;

    public function matcher(): ?RequestMatcherInterface;
}