<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Contracts\Factory;

use Thrustbit\Security\Application\Values\SecurityKey\AnonymousKey;
use Thrustbit\Security\Application\Values\SecurityKey\ContextKey;

interface FirewallContext
{
    public function anonymousKey(): ?AnonymousKey;

    public function contextKey(): ContextKey;

    public function entryPoint(): ?string;

    public function deniedHandler(): ?string;

    public function userProviderKey(): string;

    public function recaller(): array;

    public function logout(): array;

    public function isAnonymous(): bool;

    public function isStateless(): bool;

    public function hasRecaller(): bool;

    public function toImmutable(): FirewallContext;
}