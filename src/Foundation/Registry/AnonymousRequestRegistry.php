<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Registry;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Foundation\Contracts\Registry\FirewallRegistry;
use Thrustbit\Security\Application\Http\Request\Firewall\AnonymousFirewall;
use Thrustbit\Security\Application\Values\SecurityKey\AnonymousKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Provider\AnonymousAuthenticationProvider;
use Thrustbit\Security\Infrastructure\Guard\Guard;

class AnonymousRequestRegistry implements FirewallRegistry
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        if ($factory->context()->isAnonymous()) {

            $anonymousKey = $factory->context()->anonymousKey();

            $factory->registerService(
                (new PayloadFactory())
                    ->setListener($this->registerListener($anonymousKey))
                    ->setProvider($this->registerProvider($anonymousKey))
            );
        }

        return $make($factory);
    }

    private function registerListener(AnonymousKey $key): string
    {
        $id = 'firewall.anonymous.default_authentication_listener.' . $key;

        $this->container->bind($id, function (Application $app) use ($key) {
            return new AnonymousFirewall($app->make(Guard::class), $key);
        });

        return $id;
    }

    private function registerProvider(AnonymousKey $key): string
    {
        $id = 'firewall.anonymous.default_authentication_provider.' . $key;

        $this->container->bind($id, function () use ($key) {
            return new AnonymousAuthenticationProvider($key);
        });

        return $id;
    }

    public function getPriority(): int
    {
        return self::ANONYMOUS_PRIORITY;
    }
}