<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Registry;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Foundation\Contracts\Registry\FirewallRegistry;
use Thrustbit\Firewall\Foundation\Exceptions\AuthenticationExceptionHandler;
use Thrustbit\Firewall\Foundation\Exceptions\AuthorizationExceptionHandler;
use Thrustbit\Firewall\Foundation\Exceptions\ContextualSecurityHandler;
use Thrustbit\Firewall\Foundation\Exceptions\SecurityExceptionHandler;
use Thrustbit\Security\Application\Http\Response\Authorization\Contract\AuthorizationDenied;
use Thrustbit\Security\Application\Http\Response\Entrypoint\Contract\Entrypoint;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;

class FirewallExceptionHandler implements FirewallRegistry
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        $exceptionId = $this->registerExceptionListener($factory);

        $factory->registerService((new PayloadFactory())->setListener($exceptionId));

        $this->whenResolvingException(
            $exceptionId,
            $factory->aggregate()->entrypoints(),
            $factory->entrypoint()
        );

        return $make($factory);
    }

    private function registerExceptionListener(Factory $factory): string
    {
        $id = 'firewall.exception_handler.' . $factory->key();

        $this->container->bind($id, function (Application $app) use ($factory) {
            return new SecurityExceptionHandler(
                $this->contextualExceptionInstance($factory),
                $app->make(AuthenticationExceptionHandler::class),
                $app->make(AuthorizationExceptionHandler::class)
            );
        });

        return $id;
    }

    private function contextualExceptionInstance(Factory $factory): ContextualSecurityHandler
    {
        return new ContextualSecurityHandler(
            $this->container->make(Storage::class),
            new ProviderKey($factory->key()),
            $this->container->make(TrustResolver::class),
            $factory->context()->isStateless(),
            $this->resolveDefaultEntrypoint($factory->entrypoint()),
            $this->resolveDeniedHandler($factory->context()->deniedHandler())
        );
    }

    private function whenResolvingException(string $exceptionId, array $entrypoints, string $defaultEntrypoint = null): void
    {
        if (!$entrypoints) {
            return;
        }

        foreach ($entrypoints as $entrypoint) {
            if ($entrypoint === $defaultEntrypoint) {
                continue;
            }

            $this->container->resolving($exceptionId,
                function (SecurityExceptionHandler $handler, Application $app) use ($entrypoint) {
                    $handler->setEntrypoint(
                        $app->make($entrypoint)
                    );
                });
        }
    }

    private function resolveDefaultEntrypoint(string $entrypoint = null): ?Entrypoint
    {
        return $entrypoint ? $this->container->make($entrypoint) : null;
    }

    private function resolveDeniedHandler(string $deniedHandler = null): ?AuthorizationDenied
    {
        return $deniedHandler ? $this->container->make($deniedHandler) : null;
    }

    public function getPriority(): int
    {
        return self::EXCEPTION_PRIORITY;
    }
}