<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Registry;

use Illuminate\Contracts\Container\Container;
use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Foundation\Contracts\Registry\FirewallRegistry;

class EntrypointRegistry implements FirewallRegistry
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        if ($entrypoint = $factory->context()->entrypoint()) {
            $id = 'firewall.default_entry_point.' . $factory->key();

            $this->container->bindIf($id, $entrypoint);

            $factory->setEntrypoint($entrypoint);
        }

        return $make($factory);
    }

    public function getPriority(): int
    {
        return self::BOOTSTRAP_PRIORITY;
    }
}