<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Registry;

use Illuminate\Contracts\Container\Container;
use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Foundation\Contracts\Registry\FirewallRegistry;
use Thrustbit\Security\Application\Http\Request\Firewall\AccessControlFirewall;

class AccessControlRegistry implements FirewallRegistry
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        $this->container->bind($id = 'firewall.access_control', AccessControlFirewall::class);

        // fix attributes from config if exists
        $factory->registerService((new PayloadFactory)->setListener($id));

        return $make($factory);
    }

    public function getPriority(): int
    {
        return self::EXCEPTION_PRIORITY - 10;
    }
}