<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Registry;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Foundation\Application;
use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Foundation\Contracts\Registry\FirewallRegistry;
use Thrustbit\Security\Application\Http\Request\Firewall\ContextControlFirewall;
use Thrustbit\Security\Application\Values\SecurityKey\ContextKey;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;

class SerializationContext implements FirewallRegistry
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        if (!$factory->context()->isStateless()) {
            $id = 'firewall.default_context.' . $factory->key();

            $this->registerListener($id, $factory->userProvidersIds(), new ContextKey($factory->key()));

            $factory->registerService((new PayloadFactory())->setListener($id));
        }

        return $make($factory);
    }

    private function registerListener(string $id, array $userProviders, ContextKey $contextKey): void
    {
        $this->container->bind($id, function (Application $app) use ($userProviders, $contextKey) {
            foreach ($userProviders as &$userProvider) {
                $userProvider = $app->make($userProvider);
            }

            return new ContextControlFirewall(
                $app->make(Storage::class),
                $contextKey,
                $app->make(Dispatcher::class),
                $userProviders
            );
        });
    }

    public function getPriority(): int
    {
        return self::SERIALIZATION_PRIORITY;
    }
}