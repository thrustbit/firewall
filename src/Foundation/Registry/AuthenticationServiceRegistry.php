<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Registry;

use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Factory\Payload\PayloadService;
use Thrustbit\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Thrustbit\Firewall\Foundation\Contracts\Registry\FirewallRegistry;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;

class AuthenticationServiceRegistry implements FirewallRegistry
{
    /**
     * @var array
     */
    protected static $positions = ['pre_auth', 'form', 'http', 'remember_me', 'logout'];

    public function compose(Factory $factory, \Closure $make)
    {
        array_walk(self::$positions, function (string $position) use ($factory) {
            $factory
                ->make($position)
                ->map(function (AuthenticationServiceFactory $service) use ($factory) {
                    $factory->registerService(
                        $service->register(
                            $this->payload($factory, $service->userProviderKey())
                        ));
                });
        });

        return $make($factory);
    }

    protected function payload(Factory $factory, string $userProviderKey = null): PayloadService
    {
        return new PayloadService(
            new ProviderKey($factory->key()),
            $factory->context(),
            $factory->userProvider($userProviderKey),
            $factory->entrypoint()
        );
    }

    public function getPriority(): int
    {
        return self::AUTHENTICATION_SERVICE_PRIORITY;
    }

    public function getPositions(): array
    {
        return self::$positions;
    }
}