<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Registry;

use Illuminate\Contracts\Container\Container;
use Thrustbit\Firewall\Exceptions\FirewallException;
use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Foundation\Contracts\Registry\FirewallRegistry;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\Authenticatable;

class AuthenticationProviderRegistry implements FirewallRegistry
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        $bootstrapped = $make($factory);

        $this->registerSecurityAuthenticationManager(
            $factory->aggregate()->providers()
        );

        return $bootstrapped;
    }

    private function registerSecurityAuthenticationManager(array $providers): void
    {
        if (empty($providers)) {
            throw new FirewallException('Authentication providers can not be empty');
        }

        $manager = $this->container->make(Authenticatable::class);

        foreach ($providers as &$provider) {
            $provider = $this->container->make($provider);

            $manager->addProvider($provider);
        }
    }

    public function getPriority(): int
    {
        return self::BOOTSTRAP_PRIORITY - 1;
    }
}