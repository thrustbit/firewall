<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Registry;

use Illuminate\Contracts\Container\Container;
use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Factory\Manager\LogoutManager;
use Thrustbit\Firewall\Foundation\Contracts\Registry\FirewallRegistry;

class LogoutServiceRegistry implements FirewallRegistry
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var LogoutManager
     */
    private $logoutManager;

    public function __construct(Container $container, LogoutManager $logoutManager)
    {
        $this->container = $container;
        $this->logoutManager = $logoutManager;
    }

    public function compose(Factory $factory, \Closure $make)
    {
        $logout = $factory->context()->logout();

        if (!empty($logout)) {
            $this->logoutManager->addService($logout);
        }

        return $make($factory);
    }

    public function getPriority(): int
    {
        return self::BOOTSTRAP_PRIORITY - 5;
    }
}