<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Thrustbit\Security\Domain\Role\RoleHierarchy;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Voter\AnonymousVoter;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Voter\AuthenticatedTokenVoter;
use Thrustbit\Security\Infrastructure\Guard\Authorization\Voter\RoleHierarchyVoter;

class VoterServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $voters = [
        AuthenticatedTokenVoter::class,
        AnonymousVoter::class,
        RoleHierarchyVoter::class,
    ];

    public function register(): void
    {
        $this->registerVoters();

        $this->tagVoters();
    }

    protected function registerVoters(): void
    {
        $this->app->bindIf(RoleHierarchyVoter::class, function (Application $app) {
            return new RoleHierarchyVoter(
                $app->make(RoleHierarchy::class),
                (string)config('firewall.authorization.role_prefix')
            );
        });
    }

    protected function tagVoters(): void
    {
        $this->app->tag($this->voters, config('firewall.authorization.voters_tag'));
    }
}