<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Providers;

use Illuminate\Support\ServiceProvider;

class ApplicationServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->publishes(
            [__DIR__ . '/../../../config/firewall.php' => config_path('firewall.php')],
            'config');
    }

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../../../config/firewall.php', 'firewall');
    }
}