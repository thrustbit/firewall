<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Providers;

use Illuminate\Support\AggregateServiceProvider;

class FirewallAggregate extends AggregateServiceProvider
{
    protected $providers =[
        ApplicationServiceProvider::class,
        VoterServiceProvider::class,
        AuthorizationServiceProvider::class,
        SecurityServiceProvider::class,
        FirewallServiceProvider::class
    ];
}