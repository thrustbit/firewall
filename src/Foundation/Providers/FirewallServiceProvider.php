<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Thrustbit\Firewall\Factory\Config\ConfigChecker;
use Thrustbit\Firewall\Factory\Manager\AuthenticationManager;
use Thrustbit\Firewall\Factory\Manager\LogoutManager;
use Thrustbit\Firewall\Factory\Manager\RecallerManager;
use Thrustbit\Firewall\Factory\Registry;
use Thrustbit\Firewall\Foundation\Contracts\Strategy\FirewallStrategy;
use Thrustbit\Firewall\Foundation\Http\Middleware\Firewall;
use Thrustbit\Firewall\Foundation\Http\Middleware\SessionContext;

class FirewallServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function register(): void
    {
        $this->registerBootstraps();

        $this->registerMiddleware();

        $this->registerManager();
    }

    private function registerBootstraps(): void
    {
        $this->app->singleton(Registry::class, function () {
            return new Registry(config('firewall.bootstraps'));
        });
    }

    private function registerMiddleware(): void
    {
        $this->app->singleton(SessionContext::class);

        $this->app->bind(FirewallStrategy::class, config('firewall.strategy'));

        $this->app->singleton(Firewall::class);
    }

    private function registerManager(): void
    {
        $this->app->singleton(AuthenticationManager::class, function (Application $app) {
            return new AuthenticationManager(
                new ConfigChecker(config('firewall.authentication')), $app
            );
        });

        $this->app->singleton(RecallerManager::class);
        $this->app->singleton(LogoutManager::class);
    }

    public function provides(): array
    {
        return [
            Registry::class, SessionContext::class, FirewallStrategy::class,
            Firewall::class, AuthenticationManager::class, RecallerManager::class, LogoutManager::class
        ];
    }
}