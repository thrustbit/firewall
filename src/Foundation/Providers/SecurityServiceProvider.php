<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Providers;

use Illuminate\Support\ServiceProvider;
use Thrustbit\Security\Infrastructure\Guard\Authentication\AuthenticationManager;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\Authenticatable;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Contract\TrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\DefaultTrustResolver;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\AnonymousToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Contract\Storage;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\RecallerToken;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Token\Storage\TokenStorage;
use Thrustbit\Security\Infrastructure\Guard\Contracts\EventGuard;
use Thrustbit\Security\Infrastructure\Guard\Contracts\Guard as GuardContract;
use Thrustbit\Security\Infrastructure\Guard\Contracts\StorageGuard as StorageGuardContract;
use Thrustbit\Security\Infrastructure\Guard\Guard;
use Thrustbit\Security\Infrastructure\Guard\SecurityEventDispatcher;
use Thrustbit\Security\Infrastructure\Guard\StorageGuard;

class SecurityServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function register(): void
    {
        $this->app->singleton(Storage::class, TokenStorage::class);

        $this->app->bind(TrustResolver::class, function () {
            return new DefaultTrustResolver(
                AnonymousToken::class,
                RecallerToken::class
            );
        });

        $this->app->singleton(Authenticatable::class, AuthenticationManager::class);
        $this->app->bind(EventGuard::class, SecurityEventDispatcher::class);
        $this->app->bind(StorageGuardContract::class, StorageGuard::class);

        $this->app->bind(GuardContract::class, Guard::class);
    }

    public function provides(): array
    {
        return [
            Storage::class, TrustResolver::class,
            Authenticatable::class, EventGuard::class, StorageGuardContract::class,
            GuardContract::class
        ];
    }
}