<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Authentication;

use Illuminate\Contracts\Container\Container;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Thrustbit\Firewall\Exceptions\FirewallException;
use Thrustbit\Firewall\Factory\Manager\LogoutManager;
use Thrustbit\Firewall\Factory\Manager\RecallerManager;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Factory\Payload\PayloadService;
use Thrustbit\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;
use Thrustbit\Security\Application\Values\SecurityKey\ProviderKey;
use Thrustbit\Security\Application\Values\SecurityKey\RecallerKey;

abstract class RecallerAuthenticationFactory implements AuthenticationServiceFactory
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var RecallerManager
     */
    private $recallerManager;

    /**
     * @var LogoutManager
     */
    private $logoutManager;

    public function __construct(Container $container, RecallerManager $recallerManager, LogoutManager $logoutManager)
    {
        $this->container = $container;
        $this->recallerManager = $recallerManager;
        $this->logoutManager = $logoutManager;
    }

    public function register(PayloadService $payload): PayloadFactory
    {
        $recaller = $payload->context->recaller();

        if (!isset($recaller[$this->serviceKey()])) {
            throw new FirewallException(
                sprintf('Recaller key not found with service key %s', $this->key())
            );
        }

        $recallerKey = $recaller[$this->serviceKey()];

        if (!is_object($recallerKey)) {
            $recallerKey = new RecallerKey($recallerKey);
        }

        if (!$this->recallerManager->hasService($recallerKey)) {
            throw new FirewallException(
                sprintf('Recaller service not found with recaller key %s', $recallerKey));
        }

        $recallerServiceId = $this->recallerManager->getService($recallerKey);

        if ($this->logoutManager->hasService($this->serviceKey())) {
            $this->logoutManager->addHandler($recallerServiceId, $this->serviceKey());
        }

        return (new PayloadFactory)
            ->setListener($this->registerListener($recallerServiceId))
            ->setProvider($this->registerProvider($payload->firewallKey, $recallerKey));
    }

    abstract public function registerListener(string $recallerId): string;

    abstract public function registerProvider(ProviderKey $providerKey, RecallerKey $recallerKey): string;

    abstract public function serviceKey(): string;

    public function position(): string
    {
        return 'remember_me';
    }

    public function userProviderKey(): ?string
    {
        return null;
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return null;
    }
}