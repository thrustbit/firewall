<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Authentication;

use Illuminate\Contracts\Container\Container;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Thrustbit\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory as ServiceFactory;

abstract class AuthenticationServiceFactory implements ServiceFactory
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function userProviderKey(): ?string
    {
        return null;
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return null;
    }
}