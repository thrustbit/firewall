<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Authentication;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Contracts\Routing\ResponseFactory;
use Thrustbit\Firewall\Factory\Manager\RecallerManager;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Factory\Payload\PayloadRecaller;
use Thrustbit\Firewall\Factory\Payload\PayloadService;
use Thrustbit\Security\Application\Http\Response\Authentication\DefaultAuthenticationFailure;
use Thrustbit\Security\Application\Http\Response\Authentication\DefaultAuthenticationSuccess;
use Thrustbit\Security\Application\Values\SecurityKey\RecallerKey;
use Thrustbit\Security\Domain\User\Services\SecurityUserChecker;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Provider\UserNamePasswordAuthenticationProvider;

abstract class FormAuthenticationFactory extends AuthenticationServiceFactory
{
    /**
     * @var RecallerManager
     */
    private $recallerManager;

    public function __construct(Container $container, RecallerManager $recallerManager)
    {
        parent::__construct($container);

        $this->recallerManager = $recallerManager;
    }

    public function register(PayloadService $payload): PayloadFactory
    {
        $factory = (new PayloadFactory())
            ->setListener($listenerId = $this->registerListener($payload))
            ->setProvider($this->registerProvider($payload));

        $this->createRecaller($payload, $listenerId);

        return $factory;
    }

    abstract public function registerListener(PayloadService $payloadService): string;

    public function registerProvider(PayloadService $payload): string
    {
        $id = 'firewall.' . $this->position() . '.' . $this->key() . '_authentication_provider.' . $payload->firewallKey->getKey();

        $this->container->bindIf($id, function (Application $app) use ($payload) {
            return new UserNamePasswordAuthenticationProvider(
                $payload->firewallKey,
                $app->make($payload->userProviderId),
                $app->make(SecurityUserChecker::class),
                $app->make(Hasher::class),
                true
            );
        });

        return $id;
    }

    protected function registerAuthenticationSuccess(string $route): string
    {
        $id = 'firewall.' . $this->key() . '.authentication_success_handler';

        $this->container->bindIf($id, function (Application $app) use ($route) {
            return new DefaultAuthenticationSuccess(
                $app->make(ResponseFactory::class),
                $route
            );
        });

        return $id;
    }

    protected function registerAuthenticationFailure(string $route): string
    {
        $id = 'firewall.' . $this->key() . '.authentication_failure_handler';

        $this->container->bindIf($id, function (Application $app) use ($route) {
            return new DefaultAuthenticationFailure(
                $app->make(ResponseFactory::class),
                $route
            );
        });

        return $id;
    }

    private function createRecaller(PayloadService $payload, string $listenerId): void
    {
        $recaller = $payload->context->recaller();

        if (isset($recaller[$this->key()])) {
            $recallerKey = $recaller[$this->key()];

            if (!is_object($recallerKey)) {
                $recallerKey = new RecallerKey($recallerKey);
            }

            $payloadRecaller = new PayloadRecaller($payload, $recallerKey, $listenerId);

            $this->recallerManager->create($payloadRecaller);
        }
    }

    public function position(): string
    {
        return 'form';
    }
}