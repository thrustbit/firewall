<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Authentication;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Thrustbit\Firewall\Exceptions\FirewallException;
use Thrustbit\Firewall\Factory\Manager\LogoutManager;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Factory\Payload\PayloadService;
use Thrustbit\Firewall\Foundation\Contracts\Factory\AuthenticationServiceFactory;

abstract class LogoutAuthenticationFactory implements AuthenticationServiceFactory
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var LogoutManager
     */
    private $logoutManager;

    public function __construct(Container $container, LogoutManager $logoutManager)
    {
        $this->container = $container;
        $this->logoutManager = $logoutManager;
    }

    public function register(PayloadService $payload): PayloadFactory
    {
        if (!$this->logoutManager->hasService($this->serviceKey())) {
            return new PayloadFactory;
        }

        $logoutContext = $payload->context->logout();

        if (!isset($logoutContext[$this->serviceKey()])) {
            throw new FirewallException(
                sprintf('No logout service configured for service key %s', $this->serviceKey())
            );
        }

        $listenerId = $this->registerListener($payload);

        $this->addHandlersToLogoutService($listenerId);

        return (new PayloadFactory)->setListener($listenerId);
    }

    abstract public function registerListener(PayloadService $payload): string;

    abstract public function serviceKey(): string;

    protected function addHandlersToLogoutService(string $listenerId): void
    {
        $this->container->resolving($listenerId, function ($firewall, Application $app) {
            $handlers = $this->logoutManager->getHandlers($this->serviceKey());
            foreach ($handlers as &$handler) {
                $handler = $app->make($handler);

                $firewall->addhandler($handler);
            }
        });
    }

    public function position(): string
    {
        return 'logout';
    }

    public function userProviderKey(): ?string
    {
        return null;
    }
}