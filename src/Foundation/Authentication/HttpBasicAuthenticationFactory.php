<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Foundation\Authentication;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Hashing\Hasher;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Thrustbit\Firewall\Factory\Payload\PayloadFactory;
use Thrustbit\Firewall\Factory\Payload\PayloadService;
use Thrustbit\Firewall\Foundation\Http\Request\HttpBasicAuthenticationRequest;
use Thrustbit\Security\Domain\User\Services\SecurityUserChecker;
use Thrustbit\Security\Infrastructure\Guard\Authentication\Provider\UserNamePasswordAuthenticationProvider;

abstract class HttpBasicAuthenticationFactory extends AuthenticationServiceFactory
{
    public function register(PayloadService $payload): PayloadFactory
    {
        $entrypointId = $this->registerEntrypoint($payload);

        return (new PayloadFactory)
            ->setListener($this->registerListener($payload, $entrypointId))
            ->setProvider($this->registerProvider($payload))
            ->setEntrypoint($this->registerEntrypoint($payload));
    }

    public function registerProvider(PayloadService $payload): string
    {
        $id = 'firewall.http_basic_authentication_provider.' . $payload->firewallKey->getKey() . '.' . $this->key();

        // could be shared w/  U/P form auth
        $this->container->bind($id, function (Application $app) use($payload) {
            return new UserNamePasswordAuthenticationProvider(
                $payload->firewallKey,
                $app->make($payload->userProviderId),
                $app->make(SecurityUserChecker::class),
                $app->make(Hasher::class),
                true
            );
        });

        return $id;
    }

    abstract public function registerListener(PayloadService $payload, string $entrypoint): string;

    abstract public function registerEntrypoint(PayloadService $payload): string;

    public function position(): string
    {
        return 'http';
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return new HttpBasicAuthenticationRequest;
    }
}