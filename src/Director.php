<?php

declare(strict_types=1);

namespace Thrustbit\Firewall;

use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;
use Thrustbit\Firewall\Factory\Factory;
use Thrustbit\Firewall\Factory\Registry;

class Director
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var Pipeline
     */
    private $pipeline;

    public function __construct(Registry $registry, Pipeline $pipeline)
    {
        $this->registry = $registry;
        $this->pipeline = $pipeline;
    }

    public function process(Collection $authenticationServices, Request $request): Collection
    {
        return $authenticationServices->map(function (Factory $factory) use ($request) {
            return $this->pipeline
                ->via('compose')
                ->through($this->registry->getBootstraps())
                ->send($factory->setRequest($request))
                ->then(function () use ($factory) {
                    return $factory->middleware();
                });
        });
    }
}