<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Exceptions;

class FirewallException extends \RuntimeException
{
}