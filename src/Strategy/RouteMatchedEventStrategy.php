<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Strategy;

use Illuminate\Http\Request;
use Illuminate\Routing\Events\RouteMatched;

class RouteMatchedEventStrategy extends FirewallStrategy
{
    public function onEvent(RouteMatched $event): void
    {
        $this->processFirewall($event->request, $event->route->middleware());
    }

    public function delegateHandling(Request $request): void
    {
        $this->dispatcher->listen(RouteMatched::class, [$this, 'OnEvent']);
    }
}