<?php

declare(strict_types=1);

namespace Thrustbit\Firewall\Strategy;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Thrustbit\Firewall\Foundation\Contracts\Exceptions\ExceptionRegistry;
use Thrustbit\Firewall\Foundation\Contracts\Strategy\FirewallStrategy as Strategy;
use Thrustbit\Firewall\Foundation\Http\Event\FirewallHandled;
use Thrustbit\Firewall\Manager;

abstract class FirewallStrategy implements Strategy
{
    /**
     * @var Manager
     */
    private $manager;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * @var Container
     */
    private $container;

    public function __construct(Manager $manager, Router $router, Dispatcher $dispatcher, Container $container)
    {
        $this->manager = $manager;
        $this->router = $router;
        $this->dispatcher = $dispatcher;
        $this->container = $container;
    }

    protected function processFirewall(Request $request, array $middleware): void
    {
        $this->manager->raise($middleware, $request)
            ->each(function (array $middleware, string $firewallName) {
                $this->setExceptionHandler(array_pop($middleware), $firewallName);

                $this->setMiddlewareGroupToRouter($middleware, $firewallName);
            });
    }

    private function setExceptionHandler(string $exceptionHandlerId, string $firewallName): void
    {
        $exceptionHandler = $this->container->make($exceptionHandlerId);

        if (!$exceptionHandler instanceof ExceptionRegistry) {
            throw new \RuntimeException(
                sprintf('Last middleware in firewall %s must implement %s',
                    $firewallName, ExceptionRegistry::class)
            );
        }

        $this->container->make(ExceptionHandler::class)
            ->setFirewallExceptionHandler($exceptionHandler);
    }

    private function setMiddlewareGroupToRouter(array $middleware, string $firewallName): void
    {
        $this->router->middlewareGroup($firewallName, $middleware);

        $this->dispatcher->dispatch(
            new FirewallHandled($firewallName, $middleware)
        );
    }
}