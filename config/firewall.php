<?php

return [

    'strategy' => \Thrustbit\Firewall\Strategy\RouteMatchedEventStrategy::class,

    'bootstraps' => [
        \Thrustbit\Firewall\Foundation\Registry\EntrypointRegistry::class,
        \Thrustbit\Firewall\Foundation\Registry\LogoutServiceRegistry::class,
        \Thrustbit\Firewall\Foundation\Registry\AuthenticationProviderRegistry::class,
        \Thrustbit\Firewall\Foundation\Registry\SerializationContext::class,
        \Thrustbit\Firewall\Foundation\Registry\LogoutServiceRegistry::class,
        \Thrustbit\Firewall\Foundation\Registry\AuthenticationServiceRegistry::class,
        \Thrustbit\Firewall\Foundation\Registry\AnonymousRequestRegistry::class,
        \Thrustbit\Firewall\Foundation\Registry\AccessControlRegistry::class,
        \Thrustbit\Firewall\Foundation\Registry\FirewallExceptionHandler::class,
    ],

    'authentication' => [

        'user_providers' => [
            // 'service_alias' => 'service_id'
        ],

        'firewall' => [

            /* 'frontend' => [
                 // 'context' => 'Some firewall context',
                 // 'services' => ['serviceKey' => 'service factory id']
             ],
            */
        ]
    ],

    'authorization' => [

        'voters_tag' => 'firewall.authorization.voters',

        'role_prefix' => 'ROLE_',

        'role_hierarchy' => [

            'service' => \Thrustbit\Security\Domain\Role\ReachableRole::class,

            'roles' => [ /*'ROLE_ADMIN' => ['ROLE_USER']*/]
        ],

        'strategy' => \Thrustbit\Security\Infrastructure\Guard\Authorization\Strategy\UnanimousDecision::class,

        'grant' => \Thrustbit\Security\Infrastructure\Guard\Authorization\AuthorizationChecker::class,
    ]
];